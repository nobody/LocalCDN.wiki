## Welcome to the LocalCDN Wiki!

LocalCDN is a web browser extension for your Firefox that emulates CDNs (Content Delivery Networks). It intercepts the network traffic between a website and remote CDNs, finds supported resources locally, and injects them into the environment. All of this happens automatically, so no prior configuration is required.

⚠️ **Currently it's not possible to add more resources. It's currently very difficult to add more resources. Even updating the existing ones is difficult enough, as Mozilla's addon validator reaches a limit. There are too many resources included which have to be checked by Mozilla and the server crashes at some point. The problem is already known at Mozilla, but they have to change something at the infrastructure first, to validate LocalCDN correctly.**

<br>

<details>
    <summary>Here are some of the most frequently asked questions. <b>Click here to open the table of contents</b> or use the one on the right.</summary>

<ol>
<li><a href="#user-content-1-which-cdns-and-frameworks-are-supported-exactly-where-can-i-check-this">Supported CDNs and frameworks</a></li>
<li><a href="#user-content-2-can-i-use-this-extension-in-my-chrome-browser">Chrome/Chromium Browser</a></li>
<li><a href="#user-content-3-can-i-see-which-frameworks-are-requested-by-a-website-can-this-extension-also-logging">Can I see which frameworks are requested by a website? Can this extension also logging?</a></li>
<li><a href="#user-content-4-my-native-language-is-not-fully-supported-some-of-the-content-is-in-english-can-i-translate-that-somewhere">Translation</a></li>
<li><a href="#user-content-5-you-recently-changed-something-in-the-code-and-i-would-like-to-test-it-how-can-i-do-that">Test the latest changes from the main or develop branch</a></li>
<li><a href="#user-content-6-why-do-i-need-this-rule-generator-i-use-an-adblocker-and-want-to-import-these-rules-how-does-it-work">Rule generator</a></li>
<li><a href="#user-content-7-a-website-looks-weird-or-cannot-be-used-if-i-deactivate-localcdn-everything-works-what-is-the-problem">Broken websites</a></li>
<li><a href="#user-content-8-requests-to-include-a-framework-in-localcdn">Requests to include a framework in LocalCDN</a></li>
<li><a href="#user-content-9-i-have-installed-some-other-extensions-can-i-install-localcdn-is-it-compatible">Compatibility with other extensions</a></li>
<li><a href="#user-content-10-in-the-screenshots-i-see-the-dark-mode-how-can-i-activate-that">Dark Mode</a></li>
<li><a href="#user-content-11-can-i-check-if-localcdn-really-works">Can I check if LocalCDN really works?</a></li>
<li><a href="#user-content-12-localcdn-is-not-recommended-by-mozilla-is-it-safe-to-use-this-extension">LocalCDN is not recommended by Mozilla. Is it safe to use this extension?</a></li>
<li><a href="#user-content-13-can-i-use-localcdn-in-firefox-for-android-fenix">LocalCDN and Firefox for Android (Fenix)</a></li>
<li><a href="#user-content-14-i-don-t-know-your-name-how-can-i-trust-you">I don't know your name. How can I trust you?</a></li>
<li><a href="#user-content-15-what-do-you-think-about-black-lives-matter">What do you think about Black-Lives-Matter?</a></li>
<li><a href="#user-content-16-do-you-have-an-overview-of-all-donations-and-expenses">Do you have an overview of all donations and expenses?</a></li>
<li><a href="#user-content-17-what-about-fpi-and-dfpi">What about FPI and dFPI?</a></li>
<li><a href="#user-content-18-does-localcdn-change-the-fingerprint-and-make-me-uniquely-identifiable">Does LocalCDN change the fingerprint and make me uniquely identifiable?</a></li>
<li><a href="#user-content-19-is-there-a-shortcut-to-open-the-extension-menu">Is there a shortcut to open the extension menu?</a></li>
<li><a href="#user-content-20-what-are-your-recommendations-for-addons-or-settings">What are your recommendations for addons or settings?</a></li>
<li><a href="#user-content-21-enumerating-badness-vs-enumerating-goodness">Enumerating badness vs. enumerating goodness</a></li>
<li><a href="#user-content-22-stripping-metadata">Stripping metadata</a></li>
<li><a href="#user-content-23-the-date-of-the-last-update-for-the-rule-sets-is-x-months-old-is-that-normal">The date of the last update for the rule sets is X months old. Is that normal?</a></li>
<li><a href="#user-content-24-why-does-localcdn-request-the-permission-access-your-data-for-all-websites">Why does LocalCDN request the permission "Access your data for all websites"</a></li>
<li><a href="#user-content-25-what-is-this-request-was-blocked-because-the-resource-is-not-included-in-localcdn">What is "This request was blocked because the resource is not included in LocalCDN"?</a></li>
<li><a href="#user-content-26-i-ve-a-problem-with-google-services-e-g-google-maps-or-youtube">I've a problem with Google services (e.g. Google Maps or YouTube)</a></li>
<li><a href="#user-content-27-firefox-sync">Firefox Sync</a></li>
<li><a href="#user-content-28-why-are-framework-updates-no-longer-created-as-tickets-in-the-repository">Why are framework updates no longer created as tickets in the repository?</a></li>
</ol>

</details>

-----



#### 1. Which CDNs and frameworks are supported exactly? Where can I check this?
LocalCDN is Open Source so you can of course check out the code. You can either download the extension manually [here](https://addons.mozilla.org/en-US/firefox/addon/localcdn-fork-of-decentraleyes) (right click and 'Save as..') or you can check the following sections:

##### CDNs:

<img src="https://www.localcdn.org/badges/cdn.svg" alt="CDNs"> <img src="https://www.localcdn.org/badges/cdn-develop.svg" alt="CDNs (Develop branch)"><br>
You can find the supported CDNs here:
<ul>
    <li>Stable: https://codeberg.org/nobody/LocalCDN/src/branch/main/core/mappings.js</li>
    <li>Develop: https://codeberg.org/nobody/LocalCDN/src/branch/develop/core/mappings.js</li>
</ul>

##### Frameworks/Libraries:

<img src="https://www.localcdn.org/badges/frameworks.svg" alt="Frameworks (main branch)"> <img src="https://www.localcdn.org/badges/frameworks-develop.svg" alt="Frameworks (develop branch)"><br>
You can find the included Frameworks/Libraries here:
<ul>
    <li>Stable: https://codeberg.org/nobody/LocalCDN/src/branch/main/core/resources.js</li>
    <li>Develop: https://codeberg.org/nobody/LocalCDN/src/branch/develop/core/resources.js</li>
</ul>   

<br>

#### 2. Can I use this extension in my Chrome Browser?
Yes, it's possible. For publishing you need a Google Account. I don't have one and don't want one :wink: But `Emanuel Bennici` takes the source code and publish [LocalCDN in the Chrome Web Store](https://chrome.google.com/webstore/detail/localcdn-fork-from-decent/njdfdhgcmkocbgbhcioffdbicglldapd).

There are some restrictions for Chrome and Chromium based browsers. While both browsers support the WebExtensionsAPI, there are still differences. Chromium unfortunately doesn't support all features of LocalCDN.

##### Chromium incompatibilities/bugs:
<ul>
  <li>HTML filter
    <ul>
      <li>The HTML filter uses the JavaScript API <a href="https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/filterResponseData">webRequest.filterResponseData</a>. This API is not supported by Chromium. It was proposed already in 2015. I don't think this will ever work under Chromium.</li>
      <li><a href="https://bugs.chromium.org/p/chromium/issues/detail?id=487422">Chromium Bugreport | Issue 487422: WebRequest API: allow extensions to read response body</a></li>
    </ul>
  </li>
  <li>Replace/inject fonts of "Font Awesome", "Google Material Icons" (*.woff, *.woff2, *.ttf), icons and images.
    <ul>
      <li><a href="https://gitlab.com/nobody42/localcdn/-/issues/67">GitLab Issue #67</a></li>
    </ul>
  </li>
  <li>Chrome does not support to change the color of the badge text. The default color is white.
    <ul>
      <li><a href="https://codeberg.org/nobody/LocalCDN/issues/122">Issue #122</a></li>
      <li>Only one bug report exists for setBadgeTextColor (Mar 13, 2013): <a href="https://bugs.chromium.org/p/chromium/issues/detail?id=23879&q=setBadgeTextColor&can=1">Issue 23879: Finalize browser actions API (closed)</a></li>
      <li>Chromium docs: https://developer.chrome.com/extensions/browserAction</li>
      <li>MDN: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/browserAction/setBadgeTextColor</li>
    </ul>
  </li>
  <li>Lost EventListener after discarding one or more tabs
    <ul>
      <li>If a tab was discarded and later restored, no further requests can be intercepted by LocalCDN. All EventListeners have been removed by Chromium and won't be restored again. (For Firefox, the EventListeners are intact).</li>
      <li><a href="https://codeberg.org/nobody/LocalCDN/issues/1084">Issue #1084</a></li>
    </ul>
  </li>
</ul>

:warning: **Please note that I try to solve Chromium issues, but my focus is Firefox. Chromium issues have a very low priority. If you have a solution for an issue, Chromium users would be happy if you create a PR.**

<br>

#### 3. Can I see which frameworks are requested by a website? Can this extension also logging?
Of course, but logging is disabled by default.

##### 3.1. Logging by the extension

* Open the extension settings
* Click on the `Advanced` tab
* Activate the option `Enable logging`

##### 3.2. Logging by using the `browser console`

[![Enable logging ](https://www.localcdn.org/img/screenshots/how-to-logging_02_preview.png)](https://www.localcdn.org/img/screenshots/how-to-logging_02.png)


###### 3.2.2. Get logging informations
Open "Browser Console" with CTRL + SHIFT + J or the menu

[![Get logging informations ](https://www.localcdn.org/img/screenshots/how-to-logging_04_preview.png)](https://www.localcdn.org/img/screenshots/how-to-logging_04.png) [![Get logging informations ](https://www.localcdn.org/img/screenshots/how-to-logging_05_preview.png)](https://www.localcdn.org/img/screenshots/how-to-logging_05.png)

###### 3.2.3. Enable "Show Content Messages" and use the filter

[![Enable "Show Content Messages" and use the filter ](https://www.localcdn.org/img/screenshots/how-to-logging_06_preview.png)](https://www.localcdn.org/img/screenshots/how-to-logging_06.png) [![Enable "Show Content Messages" and use the filter ](https://www.localcdn.org/img/screenshots/how-to-logging_03_preview.png)](https://www.localcdn.org/img/screenshots/how-to-logging_03.png)

<br>

#### 4. My native language is not fully supported. Some of the content is in English. Can I translate that somewhere?
Yes you can and that would make me and other users very happy. LocalCDN uses the Open Source tool '[Weblate](https://hosted.weblate.org/projects/localcdn/localcdn/)' for this. You can register there and start directly or you can make suggestions (without registration). I will accept them as soon as possible.

<br>

#### 5. You recently changed something in the code and I would like to test it. How can I do that?
That would be great.

:warning: **Please use a different user profile (`about:profiles`) for this, because your previous settings will be deleted when you remove the temporary extension**

1. go to the `develop` branch and download this repository
2. unzip downloaded file to any location you want
3. start Firefox and create a new profile with `about:profiles`
4. start the new Firefox profile and open `about:debugging`
5. click `this Firefox` and `load temporary add-on...`
6. select `manifest.json` from the unzipped file

:bulb: Step-by-step video: [temporary-extension-in-firefox.mp4 (localcdn.org)](https://www.localcdn.org/temporary-extension-in-firefox.mp4)

* [Temporary installation in Firefox (extensionworkshop.com)](https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/)
* [Your first extension | Try it out - Installing (developer.mozilla.org)](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#Trying_it_out)

For Chrome and Chrome based browsers, [here](https://gitlab.com/nobody42/localcdn/-/wikis/Install-on-Chromium-based-browsers) is a guide in the wiki of the old repository.

<br>

#### 6. Why do I need this rule generator? I use an adblocker and want to import these rules. How does it work?
Your adblocker must forward the traffic to LocalCDN so that LocalCDN can detect the requests and replace them with local resources.

If you are using an adblocker (uBlock Origin, uMatrix or AdGuard) you can generate some rules in the LocalCDN settings. Please remember that these rules have to be inserted manually into your adblocker.

##### 6.1. Only uBlock Origin

For uBlock Origin the following also applies: These rules are only relevant in "medium" or "hard" mode. They are not necessary in Easy Mode or default settings. For more information, please visit the [uBlock Origin Wiki](https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode).

| Icon | Mode | Rules useful | uBlock Wiki |
| -------- | -------- | -------- | -------- |
| ![Default](https://codeberg.org/attachments/e437cff8-a289-43b2-a709-34ac0f2cffd6) | Default/Easy | No | https://github.com/gorhill/uBlock/wiki/Blocking-mode#very-easy-mode-details-
| ![Medium](https://codeberg.org/attachments/b7610b01-a2d3-411c-952a-33e9f1f53a65) | Medium | Yes | https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode |
| ![Hard](https://codeberg.org/attachments/bc68b63f-7a3f-435c-9e72-97106c7b3bf7) | Hard | Yes | https://github.com/gorhill/uBlock/wiki/Blocking-mode:-hard-mode |

Add the rules in the settings of uBlock Origin in the section **MyRules** and on the right side in the section **Temporary rules**. Then click **Save** and **Commit**. Congratulations, the rules are now permanently applied.

##### 6.2. uBlock Origin / uMatrix

Add the rules in the settings of uMatrix in the section **MyRules** and on the right side in the section **Temporary rules**. Then click **Save** and **Commit**. Congratulations, the rules are now permanently applied.

| Screenshot | Description |
| -------- | -------- |
| [<img src="https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_ublock_umatrix_1.png" alt="" width="200"/>](https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_ublock_umatrix_1.png) | Insert the generated rules under **Temporary Rules**. |
| [<img src="https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_ublock_umatrix_2.png" alt="" width="200"/>](https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_ublock_umatrix_2.png) | Click on **Save** and **Commit** |
| [<img src="https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_ublock_umatrix_3.png" alt="" width="200"/>](https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_ublock_umatrix_3.png) | Finish |

##### 6.3. AdGuard

The rules are inserted in the AdGuard settings under **User rules**. Changes are applied immediately.
[<img src="https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_adguard.png" alt="AdGuard rules" width="400"/>](https://codeberg.org/nobody/LocalCDN/raw/branch/develop/screenshots/rule_sets_adguard.png)

##### 6.4. NoScript

NoScript doesn't support importing the CDNs. So you have to export, edit and import the settings:

In your exported file you will find a section "sites" and "trusted". By default, domains/CDNs are already included there:
```
    ...
    
    "sites": {
      "trusted": [
        "§:addons.mozilla.org",
        "§:afx.ms",
        "§:ajax.aspnetcdn.com",
        "§:ajax.googleapis.com",
        "§:bootstrapcdn.com",
        "§:code.jquery.com",
        ...
```
You can attach the CDNs there. Duplicate entries are automatically removed during import.

<br>

#### 7. A website looks weird or cannot be used. If I deactivate LocalCDN, everything works. What is the problem?
There are different reasons for broken websites. LocalCDN tries to replace as many resources as possible. I think it makes no sense to replace jQuery on a website and then contact a CDN for the other 10 resources. It isn't possible to include all resources in all versions (jQuery has 151 different versions on GitHub), because that would take up too much space. That's why we need to find a compromise. The compromise in LocalCDN is replacing jQuery 3.5.0 with 3.5.1. When a web page breaks because of this, it's usually a backwards compatibility issue with the resources. So I would be happy if I get the chance to fix the problem.

* [Content Security Policy/Same Origin Policy](#7-1-sop)
* [Crossorigin/integrity attributes in HTML code](#7-2-crossorigin-integrity-attributes-in-html-code)
* [Service Worker](#7-3-service-worker)

##### 7.1. SOP
If you find error messages like this in the console (`Ctrl + Shift + K`) it looks like a CSP/SOP issue. The website determines from which sources a resource can be loaded. In this case, a resource cannot be loaded from the addon storage. The SOP (Same Origin Policy) is a security feature. A bugreport to detect this errors already exists, but with the lowest priority ([Bugzilla #1419459](https://bugzilla.mozilla.org/show_bug.cgi?id=1419459)). The only solution: Disable LocalCDN for this website.

![SOP error in browser console](https://www.localcdn.org/img/sop-error-in-browser-console.png)

##### 7.2. Crossorigin/integrity attributes in HTML code
Some websites include external frameworks and use crossorigin/integrity attributes. Both of these prevent the replacement by LocalCDN. Enable the HTML filter to remove these attributes and allow LocalCDN to replace the framework.

:warning: Unfortunately, it sometimes happens that special characters are then displayed incorrectly. In this case you can deactivate the HTML filter and/or LocalCDN, to display the umlauts correctly.

[<img src="https://www.localcdn.org/img/screenshots/screenshot9869.png" alt="Char errors" width="400"/>](https://www.localcdn.org/img/screenshots/screenshot9869.png)

###### 7.2.1. Technical details

To understand the problem, it's important to know what [character encoding](https://en.wikipedia.org/wiki/Character_encoding) is. The HTML filter reads the HTML source code with [TextDecoder](https://developer.mozilla.org/en-US/docs/Web/API/TextDecoder/TextDecoder) and the character set that's either specified in the HTML source code or transmitted by the webserver.

Then it looks for these two attributes and removes them. When everything is done, [TextEncoder](https://developer.mozilla.org/en-US/docs/Web/API/TextEncoder/TextEncoder) writes the HTML source back to the tab. That was actually all.

There is a small problem: TextDecoder can read many character sets, but TextEncoder can write UTF-8 only. This will cause errors during the conversion. These conversion errors are caused by the different character sets. The first 128 characters are the same in e.g. [ASCII](https://en.wikipedia.org/wiki/US-ASCII) and [UTF-8](https://en.wikipedia.org/wiki/UTF-8), but after that there are differences that produce these errors.

If a website is written in UTF-8, then there should be no problems with the conversion, because no conversion happens. With the other character sets it depends on which character should be displayed and if [these bits mean something different in UTF-8](http://string-functions.com/encodingtable.aspx?encoding=1258&decoding=65001).

[<img src="https://www.localcdn.org/img/screenshots/html_filter_redirections_charsets.png" alt="Technical process" width="400"/>](https://www.localcdn.org/img/screenshots/html_filter_redirections_charsets.png)


##### 7.3. Service Worker
If a website uses a service worker for caching, it can also block the replacement. If you don't need the functions of the service workers (push notifications or offline functionality), you can disable it globally in `about:config -> dom.serviceWorkers.enabled = false`

:bulb: I created an online testing program that allows you to test a website for known issues: [https://www.localcdn.org/test/check](https://www.localcdn.org/test/check)

More information about Service Workers:

* https://en.wikipedia.org/wiki/Progressive_web_application#Service_workers
* https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
* https://www.ghacks.net/2016/03/02/manage-service-workers-in-firefox-and-chrome/

##### 7.4. iframes

If the website uses [iframes](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe), it may help to add the URL of the iframe to the list of domains which the HTML filter will be applied to. Of course, this only makes sense if the HTML filter will not be applied to all websites by default.

Activate logging and open the log to find out if iframes are used. If so, then manually add the URL to the list of domains to which the HTML filter should be applied.

<br>

#### 8. Requests to include a framework in LocalCDN
Someone asked if I can add a specific framework. First of all, LocalCDN emulates CDNs to improve your online privacy and prevent cross-site profiles about you and your habits. When a website hostes these frameworks itself, then this is okay for privacy. You have no privacy advantages if you exchange the frameworks that are stored locally on the web server. The operator knows you anyway, e.g. Youtube, Twitter, Facebook and Discus. They always knows which page you are currently using.

##### 8.1. Google Fonts
Google Fonts wont be integrated, because this is not necessary to display a web page. If the web page includes Google Fonts and you block them by uBlock, the browser will use the default font instead. Apart from the fact that in my opinion it is not necessary, it is technically not possible to include all Google Fonts. The complete package of all [Google Fonts is uncompressed 950 MB (compressed 390 MB](https://github.com/google/fonts#download-all-google-fonts) in size. Just have a look into [this folder](https://github.com/google/fonts/tree/master/ofl). Extensions have a [limitation of 200 MB](https://extensionworkshop.com/documentation/publish/submitting-an-add-on/). So you have to check all Google Fonts, which is popular and which should be included and which not. I don't want to do that.

##### 8.2. YouTube/Twitter
**YouTube** doesn't include any external framework and **Twitter** is using an own CDN (abs.twimg.com). When you're using one of them only “passive”, you can try “[Invidious](https://invidio.us/)” or “[Nitter](https://nitter.net/)”, that's an alternative front-end without 3rd party scripts and CDNs. When you use Youtube/Twitter actively, it doesn't matter if the scripts are loaded locally.

##### 8.3. Google Charts
It's not allowed to use **Google Charts** "offline":

>**Can I use charts offline?**
>Your users' computers must have access to https://www.gstatic.com/charts/loader.js in order to use the interactive features of Google Charts. This is because the visualization libraries that your page requires are loaded dynamically before you use them. The code for loading the appropriate library is part of the included script, and is called when you invoke the `google.charts.load()` method. Our [terms of service](https://developers.google.com/chart/terms) do not allow you to download the `google.charts.load` or `google.visualization` code to use offline.
>
> **Can I download and host the chart code locally, or on an intranet?**
>Sorry; our [terms of service](https://developers.google.com/chart/terms) do not allow you to download and save or host the `google.charts.load` or `google.visualization` code. However, if you don't need the interactivity of Google Charts, you can screenshot the charts and use them as you wish.
>
> https://developers.google.com/chart/interactive/faq

This are the reasons why LocalCDN isn't supported now and will not be in future:

* Google Fonts
* Google Charts
* Yandex Metrika
* Google Plus One
* Twitter
* Facebook
* Disqus
* embedded self-hosted scripts from Youtube, Twitter, ...

##### 8.4. Which frameworks can be added?
Basically all that are freely available and the license terms don't forbid it. The framework must also be useful and necessary for the function of a website.


* If the website works fine and the icon is yellow, I would just ignore it. Adding missing frameworks doesn't help.
* If a website doesn't work with LocalCDN enabled, then I would first disable the "Block requests for missing resources" option. When the website works with this, it's a missing framework.
* If the website doesn't work with it, the automatic framework upgrade could be making a problem.
* It's best to briefly explain what is not working on a website. That saves me time searching for it. Depending on the website and language, this can take a long time.

<br>

#### 9. I have installed some other extensions. Can I install LocalCDN? Is it compatible?
Normally yes. At the moment I know only HTTPS Everywhere and NoScript.

##### HTTPS Everywhere

In my opinion the extension is no longer necessary due to various browser and server functions, so I haven't used it for a long time. Because I don't deal with it anymore, I can't answer any questions about it.

If you still want to use **HTTPS Everywhere**, you can find more information on this topic at [Decentraleyes](https://decentraleyes.org/configure-https-everywhere/).

In short: LocalCDN always tries to establish secure connections when requests are to be allowed through.
1. Open HTTPS Everywhere while visiting a website.
2. Disable all rules within the "Stable Rules" section, except the rule next to "localcdn.org".
3. Finish

Important: This seems to affect all browsers except Firefox and Firefox based

##### NoScript

NoScript doesn't support importing the CDNs. So you have two options:

1. allow the connection manually
2. export and import settings

The second way: You have to export, edit and re-import your NoScript configuration. In your exported file you will find a section "sites" and "trusted". By default, domains/CDNs are already included there:
```
    ...
    
    "sites": {
      "trusted": [
        "§:addons.mozilla.org",
        "§:afx.ms",
        "§:ajax.aspnetcdn.com",
        "§:ajax.googleapis.com",
        "§:bootstrapcdn.com",
        "§:code.jquery.com",
        ...
```
You can attach the CDNs there. Duplicate entries are automatically removed during import.

<br>

#### 10. In the screenshots I see the Dark Mode. How can I activate that?
This should happen automatically. If not, you will find the solution here:

* Open `about:config`
* Search for "`ui.systemUsesDarkTheme`"
* If available, change the value to `1`. If not, then create the entry (Type = Number) and set it to `1`
* :warning: Please note that this cannot work together with the setting `privacy.resistFingerprinting = true`, see #337

[<img src="https://www.localcdn.org/img/enable-dark-mode.gif" alt="Enable Dark Mode" width="500"/>](https://www.localcdn.org/img/enable-dark-mode.gif)

<br>

#### 11. Can I check if LocalCDN really works?

Yes. An external framework (e.g. jQuery) will not be listed as a network connection if LocalCDN can replace the framework. You will only find external connections there. LocalCDN redirects to a local file, however.

<img src="https://www.localcdn.org/img/screenshots/screenshot9893.png" alt="How can I check if LocalCDN really works?" width="500"/>

You can see the redirection through the source code. On the web page just right click and select "View Page Source". In the source code you will find e.g. this:

`<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>`

If you copy this link, paste it into the address line, you will be redirected to the local file. Please do not just click on the link, because this will redirect you to this:

`view-source:https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js`

If you open `https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js`, you will be forwarded to `moz-extension://e129e293-bad9-44d2-ab61-9a5cacb106be/resources/jquery/2.2.4/jquery.min.jsm`. The extension ID (`e1..be`) is random.

Example: https://www.localcdn.org/dl/how_can_i_check_if_localcdn_really_works.mp4

<br>

#### 12. LocalCDN is not recommended by Mozilla. Is it safe to use this extension?

I don't know the exact definitions for "Recommended" and how often this will be checked. Definitely not with every update. The badges are also no guarantee that an extension protect your privacy. Do you remember "Web of Trust"? These extension was also recommended by Mozilla for a while. So you can use the badges as a orientation, but it isn't a guarantee.

I wrote Mozilla an e-mail about this a few weeks ago, but they still haven't answered me. Maybe Mozilla is busy at the moment because of less employees, Fenix and [the new badges](https://blog.mozilla.org/addons/2020/10/05/new-add-on-badges/) (I already applied for it)

Mozilla has published [here](https://support.mozilla.org/en-US/kb/tips-assessing-safety-extension) some tips about the missing badges. These badges are only available for Firefox. Chromium has no such labels at all.

If you have a question about a certain line of code or a certain section of code, just create a ticket and I will answer the question.

<br>

#### 13. Can I use LocalCDN in Firefox for Android (Fenix)?

Yes. Mozilla unlocked LocalCDN and over 400 other extensions for [Firefox on Android](https://play.google.com/store/apps/details?id=org.mozilla.firefox) v120, which means we don't have to wait for [Firefox on Android](https://play.google.com/store/apps/details?id=org.mozilla.firefox) v121. LocalCDN is available now.

Forks ([Fennec](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/), [Mull](https://f-droid.org/packages/us.spotco.fennec_dos/)) have to wait for Firefox on Android v121 to use all the addons again, but this will be the case in a few days, so the workaround via the collection is obsolete. 

<br>

#### 14. I don't know your name. How can I trust you?

The skepticism is good and should always be the case. There aren't many developers who publish more than their name. A good example are Custom ROMs. Many times I see only a name and maybe a country, but nothing more. As a developer you also want to protect your privacy, because something once published on the internet cannot be removed. If you publish your name nobody will check it. So I can write what I want. If you just need a name to trust me, just call me Marc :wink:

Privacy is important for me. Mine but also yours. I like it when you report missing frameworks, bugs or suggestions on Codeberg, because I delete emails automatically after 14 days. If you report something to me via email, I have to add it manually on Codeberg. Why do I do that? If a missing framework was reported by e-mail and I want to visit this website a second time, I don't know which website it is after 14 days. Suggestions and bug reports should also be public. You do not have to write long text.

Instead of publishing my name, I do other things that might be important:
* no external connections by LocalCDN
* internal statistics of LocalCDN disabled by default (will not be transmitted anyway)
* small commits to better understand code changes
* all commits are signed with GPG
* The translation platform is Weblate (Open Source) and translations without registration possible
* Codeberg instead of GitHub, GitLab or other platforms that collect and analyze your data
* The website (www.localcdn.org) doesn't use cookies, tracking or analysis tools
* Server logfiles are automatically deleted after 5 days
* Emails are automatically deleted after 14 days
* Contact by e-mail with PGP encryption possible
* Strong CSP (= Content Security Policy) for extension
    * See [manifest.json](https://codeberg.org/nobody/LocalCDN/src/commit/3aa3e820780ecc45524ab3c92f44e76a47ed84e0/manifest.json#L61)
    * More information about CSP can be found [here](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/content_security_policy)

<br>

#### 15. What do you think about Black-Lives-Matter?

People who know me know that **I'm absolutely against racism**. We are all human. Right now we are alone in this huge universe. Every human being deserves to be respected. No matter what skin color, nationality, gender, religion or anything else. Racism is stupid.

The branch name was changed from Master to Main some time ago. I know that the source code of LocalCDN contains discriminating terms like whitelist and blacklist. I will change these terms to "Allowlist" and "Blocklist". Unfortunately this isn't easy because I used the same terms for the extension storage. I could change this in the next version, but if a user skips exactly this update, it could cause problems. So yes, I will change that, but it takes some time. Issue that containing these changes: #138

No matter if before or after this change: I'm against racism.

Of course I also hate all other stupid things that harm or disadvantage another person (war, terror, violence, etc.)

My message to all racists and all who harm or disadvantage people: :fu:

<br>

#### 16. Do you have an overview of all donations and expenses?

Yes, you can find this overview [here](https://codeberg.org/nobody/LocalCDN/wiki/Donations-and-expenses).

<br>

#### 17. What about FPI and dFPI?

FPI (= first party isolation) and dFPI (= dynamic first party isolation) are a very good and useful things to make cross-site tracking more difficult. However, both have no effect on the connection itself and the external server. What happens there cannot be modified by the browser. This is the same reason why you probably use an adblocker, disable Android's Captive Portal Check or use your own DNS server. Unfortunately, you can't completely block this connection to a CDN, because then a website probably won't work. LocalCDN forwards these requests to the local addon storage, so the website will work after all. Of course you can use both technologies. I would always enable (d)FPI and disable LocalCDN only when necessary.

If external connections don't bother you, you should ask yourself if you would install an extension that pings a server for every website or if you would use a Google time server on all your devices. If this ping does not bother you, then LocalCDN is not recommended for you.

A nice little side effect: A CDN delivers resources to multiple websites and is therefore an interesting target. It's possible that a bad guy could inject a manipulated resource into the CDN eco system and compromise multiple websites at the same time. Security researcher RyotaK found a way to compromise the whole Cloudflare CDNJS network in July 2021 [(www.bleepingcomputer.com)](https://www.bleepingcomputer.com/news/security/critical-cloudflare-cdn-flaw-allowed-compromise-of-12-percent-of-all-sites/). 

This example shows how dangerous external connections can be. You should therefore eliminate any connection that is not necessary. A CDN is not necessary from a technical point of view. The operator of a website can also host the stuff himself. So if you also want to reduce the number of external connections, LocalCDN makes sense.

<br>

#### 18. Does LocalCDN change the fingerprint and make me uniquely identifiable?

Some users asked if LocalCDN changes the fingerprint. I have been working on this question for a while. Until now I don't know how to get a different fingerprint through LocalCDN. I don't know any theoretical example and also no [PoC](https://en.wikipedia.org/wiki/Proof_of_concept).

It would be possible to check the [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier) and see how many extensions with [`web_accessible_resources`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/web_accessible_resources) are installed. The problem: the UUIDs are random with each extension and Firefox profile. A script would therefore have to try all possible combinations and for each UUID found (with web_accessible_resources) test all known extension files contained there. This takes a very very long time because there are a lot of UUID combinations: 16^32 = 340,282,366,920,938,463,463,374,607,431,768,211,456


<br>

#### 19. Is there a shortcut to open the extension menu?

Yes. In version 2.6.13 there was the combination Ctrl+Shift+L. In newer versions no combination was set by default. Follow these instructions to change or set this shortcut: https://support.mozilla.org/en-US/kb/manage-extension-shortcuts-firefox

<br>

#### 20. What are your recommendations for addons or settings?

First of all: It **depends on you and your habits!**

##### user.js
Use the good customizability of Firefox and use a user.js. Which settings you set there is also up to you. A good starting point is e.g. [www.privacy-handbuch.de](https://www.privacy-handbuch.de/handbuch_21u.htm) (German). Find out the **best for your case**, test it and use it. You have to understand what each setting does.

##### uBlock Origin
No idea, but probably other adblockers are also good. I'm very satisfied with uBlock Origin. At the moment I've no reason to change. The [medium mode](https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode) there is really good. I use it on almost all devices. One device even uses [hard mode](https://github.com/gorhill/uBlock/wiki/Blocking-mode:-hard-mode). But it **depends on the websites** you visit.

##### Firefox Profiles
Use the possibility to create different profiles in Firefox. To prevent confusion, you can use them with different themes.
A temporary profile is sometimes also useful. I've a small bash script that creates such a profile for me. After closing the browser the complete profile folder will be deleted:
```
#!/bin/bash
PROFILEDIR=`mktemp -p /tmp -d tmp-fx-profile.XXXXXX.d`
/path/to/firefox/firefox -profile $PROFILEDIR -no-remote -new-instance
rm -rf $PROFILEDIR
```

##### Are the addons from the collcetion all tested, trusted or recommended?
No. Some of the addons I actually use myself. Others are only in the list because someone else asked if I could add them.

<br>

#### 21. Enumerating badness vs. enumerating goodness

`Enumerating badness` is a bad thing, because there are more bad domains than good domains. You should only allow that which is absolutely necessary. The opposite of enumerating badness is enumerating goodness and means only necessary connections should be allowed, e.g. with uBlock Origin in medium mode. A CDN was useful in the past, but today CDNs are rarely necessary. Low latency or geographical proximity won't be noticed with such small files. Therefore, I think that this connection is not necessary from a technical point of view. The website owners can store the stuff on their own web server. My recommendation: uBlock Origin in Medium Mode and LocalCDN. Activate `Block requests for missing resources` in the LocalCDN settings. 

<br>

#### 22. Stripping metadata

The HTTP headers [Cookie](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cookie), [Origin](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Origin) and [Referer](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referer) are legitimate header, but mostly they are used in such a way the privacy might be violated. The Referer header in the first place. LocalCDN catches all HTTP headers before submitting them and removes just these three headers. However, some users reported this could be problematic with Google services. In this case you have to disable this feature.

Please note that LocalCDN does not remove these headers from **all** requests. It only removes the headers from the requests which goes to a CDN like `ajax.googleapis.com` or `cdnjs.cloudflare.com`. Some parts could also be configured for Firefox with `user.js` globally for all domains.

<br>

#### 23. The date of the last update for the rule sets is X months old. Is that normal?

Yes, it is. The full path of a resource is not important for the rule sets, only the root domain is important.

Example:

```
cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js
^^^^^^^^^^^^^^^^^^^^
     important
      

cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                  unimportant
```

You can see this also in the generated rules: `* cdnjs.cloudflare.com * noop`

<br>

#### 24. Why does LocalCDN request the permission "Access your data for all websites"?

With the introduction of MV3, this permission is listed as an optional permission. Before MV3 this permission was requested once during the installation (e.g. LocalCDN [v2.6.43](https://codeberg.org/nobody/LocalCDN/src/commit/95df4e8afd19891bdd5eb07616abb5d59a4cbef4/manifest.json#L24) or [v2.6.5](https://codeberg.org/nobody/LocalCDN/src/commit/eb11dd311219caa4f9a5c59ee39ce4fcdd79da99/manifest.json#L24)). Now this permission must be requested additionally.

LocalCDN needs to know which requests are generated by a website in order to redirect or block them. It's possible to restrict this permission to single sites, e.g. `mozilla.org`. But then LocalCDN can only process the requests that are triggered by `mozilla.org`. This makes no sense because all requests should be monitored. Unfortunately, this permission is very broad and is not precisely configurable.

(This changes in the firefox permission system affects LocalCDN v3.0.0 and higher)

<br>

#### 25. What is "This request was blocked because the resource is not included in LocalCDN"?

When you type the URL of a resource into the address bar of the browser, this message appears:

[<img src="https://www.localcdn.org/img/blocked-by-localcdn.png" alt="" width="200"/>](https://www.localcdn.org/img/blocked-by-localcdn.png)

This is to prevent opening a resource from a CDN by mistake. If you still want to open a file there in the browser, then add the domain in the settings under "Deactivate LocalCDN for these domains".

This will allow you to open the file in the browser, but other requests will still be blocked or allowed (depending on the setting of "Block requests for missing resources").

Example: If you want to open the file `https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.2.0/chart.min.js` in the browser tab, then add the domain `cdnjs.cloudflare.com` in the settings.

<br>

#### 26. I've a problem with Google services (e.g. Google Maps or YouTube)

---

⚠️ Normally, this section should no longer be necessary starting with LocalCDN v2.6.58.

Google Fonts are permanently allowed on Google websites (Maps, G-Mail, YouTube, etc.) because no one reads through this wiki anyway or takes a serious look at the addons which they install. There are always users who are completely surprised to discover that LocalCDN blocks Google Fonts by default. These kinds of reports reach me again and again and every time I have to explain what the addon does and where they can change it. To reduce these frustrating tasks (why the hell is anyone blindly installing any addons??), Google services are allowed to load the Google fonts.

The Google domains are listed [here](https://codeberg.org/nobody/LocalCDN/src/commit/5ee66f2a99355b731b0d7c28fb8a7c5bdc635706/core/constants.js#L456). I think this should cover most of the domains. Attention: Permalink! Manually switch to the `main` branch and scroll to the `GoogleDomains` constant to see the current list.

---

Yes, that's normal for Google services (not only Maps), because in the default settings Google Fonts are blocked. For an addon designed to improve privacy, this is the only logical default option. (The shitstorm would be gigantic if this option was allowed by default :wink: )

* If certain things **are not displayed**, it's usually caused by the Google Fonts which cannot be downloaded because they are blocked by LocalCDN.
  * Solutions:
    * Add this domain `*.google.com` to the allowed list for Google Fonts.
    * Disable LocalCDN for the Google website.
* If things are **displayed incorrectly**, then the Google Fonts could be loaded, but Google uses an exclusive set of symbols.
  * Solutions:
    * Add this domain `*.google.com` to the allowed list for Google Fonts.
    * Disable LocalCDN for the Google website.
* If something **fails to load**, then it could also be caused by the stripped metadata.
  * Solutions:
    * Disable the metadata stripping in the options for all websites.
    * Disable LocalCDN for the Google website.
    
I've received feedback from a Google user. I can't evaluate that because I have no experience with Google services:

> I think Google has changed something. The described problem with the control elements is only since two or three weeks. Before that, Google Maps with LocalCDN worked without problems.

<br>

#### 27. Firefox Sync

The Sync option has been marked as beta since August 2023. Although nothing has changed in the Firefox API, the browser regularly gives errors there. In my last tests a few months ago, it worked fine. Either this is a bug in the sync store or something has been fundamentally changed.

I think the best thing would be to rebuild this feature at some point and define a separate storage, e.g. Nextcloud. Pull requests are welcome if someone wants to do that. ([#1510](https://codeberg.org/nobody/LocalCDN/issues/1510))

<br>

#### 28. Why are framework updates no longer created as tickets in the repository?

Codeberg has a rate limit for creating tickets since a while. When I'm updating frameworks, I regularly hit this limit and have to stop working until I can create issues again to push the code changes. To prevent stopping work every time, no issues will be created from now on for simple framework updates. The code changes are still traceable, however, because the commit in the release notes will always be framework-related. Only the link to the ticket is missing and there the link to the milestone.