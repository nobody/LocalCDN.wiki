First of all, LocalCDN emulates CDNs to improve your online privacy and prevent cross-site profiles about you and your habits. When a website hostes these frameworks itself, then this is okay for privacy. You have no privacy advantages if you exchange the frameworks that are stored locally on the web server. The operator knows you anyway, e.g. Youtube, Twitter, Facebook and Discus. They always knows which page you are currently using.

Google Fonts will not be integrated, because this is not necessary to display a web page. If the web page includes Google fonts and you block them by uBlock, the browser will use the default font instead.

Youtube doesn't include any external framework and Twitter is using an own CDN (abs.twimg.com). When you're using one of them only "passive", you can try "invidious" or "Nitter", that's an alternative front-end without 3rd party scripts and CDNs. When you use Youtube/Twitter actively, it doesn't matter if the scripts are loaded locally.

This are the reasons why LocalCDN isn't supported now and will not be in future:

* Google Fonts
* Yandex Metrika (watch.js)
* Google Plus One (plusone.js)
* Twitter widgets (widgets.js, client.js)
* Facebook (en_US/all.js)
* Disqus
* embedded self-hosted scripts from Youtube, Twitter, ...