In some cases the layout of a website is broken, even the add-on is working. 

The problem isn't caused by CDNs, any frameworks or LocalCDN, but of the website that includes the external JavaScript/CSS file. In the console of your browser (`Ctrl + Shift + K`) you can find error messages like this:

![Bildschirmfoto_2020-03-11_09-41-00](uploads/39bfd023e2fedffc56dbf01b9881eca8/Bildschirmfoto_2020-03-11_09-41-00.png)

At the moment I haven't found a way to fix this, because the SOP (Same Origin Policy) is a security feature:

>The same-origin policy is a critical security mechanism that restricts how a document or script loaded from one origin can interact with a resource from another origin. It helps isolate potentially malicious documents, reducing possible attack vectors.

A bugreport already exists, but with the lowest priority. See [Bugzilla #1419459](https://bugzilla.mozilla.org/show_bug.cgi?id=1419459)

More informations about [SOP (Same-origin policy)](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy)

--

**Update 2020-05-10 (LocalCDN v2.2.0)**

In many cases this has been fixed (#66) by modifying the HTML source code of a website. This results in minimal extended loading times when a web page is called, but this should not be noticeable.

However, there are still some websites which use strict SOPs and still cause problems because the SOP problem still exists.